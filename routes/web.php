<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@getIndex')->name('home.index');
Route::post('/login', 'HomeController@postLogin');
Route::get('/register', 'HomeController@getRegister')->name('home.register');
Route::post('/register', 'HomeController@postRegister');
Route::get('/token', function ()
{
	return csrf_token();
});

Route::group(array('prefix' => 'user', 'as' => 'user.'), function()
{
	Route::get('/', 'ChannelsController@index')->name('index');
	Route::get('index', 'ChannelsController@index')->name('index');
	Route::get('channel/{id}', 'ChannelsController@show')->name('channel');
	Route::post('channel/create', 'ChannelsController@store');
	Route::patch('channel/update/{id}', 'ChannelsController@update');
	Route::post('channel/delete', 'ChannelsController@delete');

	Route::post('field/create', 'FieldsController@store');
	Route::get('field/{id}', 'FieldsController@show');
	Route::patch('field/update/{id}', 'FieldsController@update');
	Route::post('field/delete', 'FieldsController@delete');
});

Route::group(array('prefix' => 'api', 'as' => 'api.'), function(){
	// Route::get('channels/{channel_id}', 'ApiController@getChannel');
	Route::get('channels/{channel_id}/fields/{field_id}', 'ApiController@getChannelField');
	Route::get('channels/{channel_id}/fields/{field_id}/last', 'ApiController@getChannelFieldLast');
	Route::get('channels/update', 'ApiController@getUpdate');
});