<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
	protected $table = 'fields';

    public function details()
    {
    	return $this->hasMany('App\FieldDetail', 'field_id');
    }

    public function last()
    {
    	return $this->hasMany('App\FieldDetail', 'field_id')->latest();
    }

    public function channel()
    {
    	return $this->belongsTo('App\channel', 'channel_id');
    }
}
