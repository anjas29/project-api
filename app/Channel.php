<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $table = 'channels';

    protected $hidden = array('write_key', 'read_key');

    public function fields()
    {
    	return $this->hasMany('App\Field', 'channel_id')->orderBy('created_at', 'desc');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
