<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Field;
use App\FieldDetail;
use App\Channel;

class FieldsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $fields = Field::where('channel_id', $id)->get();

        return view('user.fields.index')->withFields($fields);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $channel = Channel::where('id', $request->input('channel_id'))->first();
        $channel->field_count = $channel->field_count + 1;
        $channel->save();

        $data = new Field;
        $data->channel_id = $request->input('channel_id');
        $data->name = $request->input('name');
        $data->field_number = 'field'.$channel->field_count;
        $data->save();        

        return redirect('/user/channel/'.$request->input('channel_id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = FieldDetail::where('field_id', $id)->get();
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Field::where('id', $id)->first();
        $data->name = $request->input('name');
        $data->save();

        return redirect('/user/channel/'.$request->input('channel_id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function delete(Request $request)
    {
        $data = Field::where('id', $request->input('id'))->first();
        $data->delete();

        $channel = Channel::where('id', $request->input('channel_id'))->first();
        $channel->field_count = $channel->field_count - 1;
        $channel->save();

        return redirect('/user/channel/'.$request->input('channel_id'));
    }
}
