<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth:web', ['except' => $sites]);
	}

	public function getIndex()
	{		
		return view('user.dashboard.index');
	}
}
