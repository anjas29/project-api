<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Channel;
use App\Field;

class ChannelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $profil = auth()->user();
        $channels = Channel::where('user_id', $profil->id)->orderBy('created_at', 'DESC')->get();
        $channels_count = $channels->count();
        
        $field_count = Field::with(array('channel.user' => function($query, $profil){
            $query->where('user.id', $profil->id);
        }))->count();

        $count = array('channels' => $channels_count, 'fields' => $field_count);

        return view('user.channels.index')->withChannels($channels)->withProfil($profil)->withCount($count);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Channel;
        $data->user_id = auth()->user()->id;
        $data->name = $request->input('name');
        $data->description = $request->input('description');
        $data->write_key = strtoupper(str_random(30));
        $data->read_key = strtoupper(str_random(30));
        $data->public = false;
        $data->save();

        return redirect(route('user.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Channel::where('id', $id)->with('fields')->first();

        return view('user.channels.show')->withData($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {  
        $data = Channel::where('id', $id)->first();
        $data->name = $request->input('name');
        $data->description = $request->input('description');
        $data->public = $request->input('public');
        $data->save();

        return redirect(route('user.channel'));
    }

    public function genereateWriteKey(Request $request, $id)
    {
        $data = Channel::where('id', $id)->first();
        $data->write_key = strtoupper(str_random(30));
        $data->save();

        return redirect(route('user.channel'));
    }

    public function genereateReadKey(Request $request, $id)
    {
        $data = Channel::where('id', $id)->first();
        $data->read_key = strtoupper(str_random(30));
        $data->save();

        return redirect(route('user.channel'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Channel::where('id', $id)->first();
        $data->delete();

        return redirect(route('user.channel'));
    }

    public function delete(Request $request)
    {   
        $id = $request->input('id');
        $data = Channel::where('id', $id )->first();
        $data->delete();

    }
}
