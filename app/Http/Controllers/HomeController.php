<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
use Session;

class HomeController extends Controller
{
	public function getIndex()
    {
    	$auth = auth('users');

        if($auth->check()){
            return redirect(route('user.index'));
        }

        return view('login');
    }

    public function postLogin(Request $request)
    {
   
    	$auth = auth('users');

        $credential = array(
            'email' => $request->input('email'),
            'password' => $request->input('password')
        );

        if($auth->attempt($credential)){
        	Session::flash('success', '');
            return redirect(route('user.index'));
        }else{
            Session::flash('failed', '');
            return redirect(route('home.index'));
        }

        return redirect(route('home.index'));
    }

    public function getRegister()
    {
        return view('register');
    }

    public function postRegister(Request $request)
    {
        $data = new User;
        $data->email = $request->input('email');
        $data->password = Hash::make($request->input('password'));
        $data->name = $request->input('name');
        $data->api_token = str_random(60);
        $data->save();

        Session::flash('success','Registration Success!');
        return redirect(route('home.index'));
    }

    public function getLogout()
    {
    	auth('users')->logout();
    	return redirect(route('home.index'));
    }
}
