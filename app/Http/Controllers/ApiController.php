<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Channel;
use App\Field;
use App\FieldDetail;

class ApiController extends Controller
{
    public function getChannelField(Request $request, $channel_id, $field_id)
    {
    	$data = null;

    	$read_key	 = $request->input('api_key');

    	$channel = Channel::where('read_key', $read_key)->first();

    	if($channel_id != $channel->id){
    		return array("success" => false, "description" => "Invalid API Key!");
    	}

    	if($channel != null){

    		$data = Field::where('field_number', $field_id)
    						->where('channel_id', $channel->id)->with('details')->first();
    		if($data != null){
    			return array('success' => true,
                     'data' => $data
                    );	
    		}else{
    			return array("success" => false, "description" => "Invalid Field Name!");
    		}
    		

    	}else{
    		return array("success" => false, "description" => "Invalid API Key!");
    	}

    	return array("success" => false, "description" => "Invalid API Key!");

    	
    }

    public function getChannelFieldLast(Request $request, $channel_id, $field_id)
    {
    	$data = null;

    	$read_key	 = $request->input('api_key');

    	$channel = Channel::where('read_key', $read_key)->first();

    	if($channel_id != $channel->id){
    		return array("success" => false, "description" => "Invalid API Key!");
    	}

    	if($channel != null){

    		$data = Field::where('field_number', $field_id)
    						->where('channel_id', $channel->id)->with(array('last' => function($query){
    							$query->first();
    						}))->first();
    		if($data != null){
    			return array('success' => true,
                     'data' => $data
                    );	
    		}else{
    			return array("success" => false, "description" => "Invalid Field Name!");
    		}
    		

    	}else{
    		return array("success" => false, "description" => "Invalid API Key!");
    	}

    	return array("success" => false, "description" => "Invalid API Key!");

    	
    }

    public function getUpdate(Request $request)
    {
    	$data = null;
    	$write_key = $request->input('api_key');

    	$channel = Channel::where('write_key', $write_key)->first();

    	
    	if($channel != null){
    		$entry_count = 0;
    		for ($i=1; $i <= 100; $i++) { 
    			$field = Field::where('field_number', 'field'.$i)
    							->where('channel_id', $channel->id)->first();
	    		if($field != null && $request->input('field'.$i) != null){
	    			$data = new FieldDetail;
	    			$data->field_id = $field->id;
	    			$data->value = $request->input('field'.$i);
	    			$data->latitude = $request->input('latitude');
	    			$data->longitude = $request->input('longitude');
	    			$data->device_code = $request->input('device_code');
	    			$data->save();
	    			$entry_count++;
	    		}
    		}

    		if($entry_count == 0){
				return array("success" => false, "description" => "There is some invalid field name in process!");	
			}else{
				return array("success" => true, "description" => "Update Data Success!");	
			}
    	}else{
    		return array("success" => false, "description" => "Invalid API Key!");
    	}

    	return array("success" => false, "description" => "Invalid API Key!");
    }
}
