<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FieldDetail extends Model
{
    protected $table = 'field_details';

    public function field()
    {
    	return $this->belongsTo('App\Field', 'field_id');
    }
}
