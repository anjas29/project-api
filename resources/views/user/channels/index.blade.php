@extends('layouts.user_layout')
@section('css')
<link rel="stylesheet" type="text/css" href="/css/dataTables.bootstrap.min.css">
@stop
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard
    </h1>
    <ol class="breadcrumb">
      <li><a href="/user/index"><i class="fa fa-home"></i> Dashboard</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-success">
          <div class="box-header with-border">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Profile</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="box-body">
            <div class="col-md-6">
              <h4 style="margin-top: 0px; padding-top: 0px;">Detail</h4>
              <ul class="list-group list-group-unbordered" style="margin: 0px;">
                <li class="list-group-item">
                 <strong><i class="fa fa-user margin-r-5"></i> Name</strong>
                 <a href="#" class="text-muted pull-right">{{$profil->name}}</a>
               </li>
               <li class="list-group-item">
                 <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>
                 <a href="#" class="text-muted pull-right">{{$profil->email}}</a>
               </li>              
             </ul>
           </div>

           <div class="col-md-6">
             <h4 style="margin-top: 0px; padding-top: 0px;">Counts</h4>
             <div  class="row">
              <div class="col-md-6">
                <div class="info-box bg-blue">
                  <span class="info-box-icon"><i class="fa fa-bar-chart"></i></span>
                  <div class="info-box-content">
                    <span class='info-box-text'>Channels</span>
                    <span class="info-box-number">{{$count['channels']}}</span>
                    <span class="progress-description" style="margin-top: 12px;">
                      <a href="#" style="text-decorations:none; color:inherit;"><b>More Info</b></a>
                    </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <div class="col-md-6">
                <div class="info-box bg-yellow">
                  <span class="info-box-icon"><i class="fa fa-cubes"></i></span>
                  <div class="info-box-content">
                    <span class='info-box-text'>Active Fields</span>
                    <span class="info-box-number">{{$count['fields']}}</span>
                    <span class="progress-description" style="margin-top: 12px;">
                      <a href="#" style="text-decorations:none; color:inherit;"><b>More Info</b></a>
                    </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-bar-chart"></i>
          <h3 class="box-title">
            Channels 
            <button type="button" class="btn btn-xs btn-primary" style="margin-left: 5px;" data-toggle='modal' data-target="#createModal">Create</button>
          </h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>

        <div class="box-body no-padding">
          <div class="col-md-12">
            <table class="table table-striped table-bordered dataTable">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Created</th>
                  <th>Last Update</th>
                  <th>Action</th>
                </tr>                  
              </thead>
              <tbody>
              @foreach($channels as $d)
                <tr>
                  <td class="col-md-6">
                    <b>{{$d->name}}</b>
                    <p>{{$d->description}}</p>
                  </td>
                  <td>{{$d->created_at}}</td>
                  <td>{{$d->updated_at}}</td>
                  <td>
                    <a href="/user/channel/{{$d->id}}" class="btn btn-xs btn-primary" data-id='{{$d->id}}' data-name='{{$d->name}}'><i class="fa fa-eye"></i></a>
                    <a href="#" class="delete btn btn-xs btn-danger" data-id='{{$d->id}}'><i class="fa fa-times"></i></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
    <!-- End Row-->
  </div>
  <!-- End Row-->
  <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- detail News Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id='createModal'>
    <div class="modal-dialog" role="document">
      <form action="/user/channel/create" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <strong>Create Channel</strong><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                  <input type="text" class="form-control" placeholder="Name" name="name">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-file-text-o"></i></span>
                  <textarea class="form-control" placeholder="Decsription" name="description"></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              {{csrf_field()}}
              <button type="button" class="btn btn-sm" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-sm btn-primary pull-right">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

@endsection
@push('js')
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>

<script type="text/javascript">
  $('.delete').click(function() {

        var id = $(this).data('id');

        var _token = '{{csrf_token()}}';

        bootbox.confirm("<b>Are you sure to delete this Channel</b>?", function(result) {
          if (result) {
            toastr.options.timeOut = 0;
            toastr.options.extendedTimeOut = 0;
            toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Process...');
            toastr.options.timeOut = 5000;
            toastr.options.extendedTimeOut = 1000;
            $.post("/user/channel/delete", {id: id, _token:_token})
            .done(function(result) {
              window.location.replace("/user/index");
            })
            .fail(function(result) {
              toastr.clear();
              toastr.error('Server Error! Please reload this page again.');
            });
          };
        });
      });
</script>
@endpush