@extends('layouts.user_layout')
@section('css')
<link rel="stylesheet" type="text/css" href="/css/dataTables.bootstrap.min.css">
@stop
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Channel
    </h1>
    <ol class="breadcrumb">
      <li><a href="/user/channel/{{$data->id}}"><i class="fa fa-bar-chart"></i> Channel</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-6">
        <div class="box box-success">
          <div class="box-header with-border">
            <i class="fa fa-cubes"></i>
            <h3 class="box-title">
              Detail
            </h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>

          <div class="box-body no-padding">
            <div class="col-md-12">
              <ul class="list-group list-group-unbordered" style="margin: 0px;">
                <li class="list-group-item">
                 <strong><i class="fa fa-hashtag margin-r-5"></i> Channel ID</strong>
                 <a href="#" class="text-muted pull-right">{{$data->id}}</a>
                </li> 
                <li class="list-group-item">
                 <strong><i class="fa fa-cube margin-r-5"></i> Name</strong>
                 <a href="#" class="text-muted pull-right">{{$data->name}}</a>
               </li>             
             </ul>
             <br>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="box box-warning">
          <div class="box-header with-border">
            <i class="fa fa-key"></i>
            <h3 class="box-title">
              API Key
            </h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>

          <div class="box-body no-padding">
            <div class="col-md-12">
             <ul class="list-group list-group-unbordered" style="margin: 0px;">
                <li class="list-group-item">
                 <strong><i class="fa fa-pencil margin-r-5"></i> Write API Key</strong>
                 <a href="#" class="text-muted pull-right"><b>{{$data->write_key}}</b></a>
                </li> 
                <li class="list-group-item">
                 <strong><i class="fa fa-eye margin-r-5"></i> Read API Key</strong>
                 <a href="#" class="text-muted pull-right"><b>{{$data->read_key}}</b></a>
               </li>             
             </ul>
             <br>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <i class="fa fa-cubes"></i>
            <h3 class="box-title">
              Fields
              <button type="button" class="btn btn-xs btn-primary" style="margin-left: 5px;" data-toggle='modal' data-target="#createModal">Create Field</button>
            </h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>

          <div class="box-body no-padding">
            <div class="col-md-12">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>Field Name</th>
                    <th>Name</th>
                    <th>Created</th>
                    <th>Last Update</th>
                    <th>Action</th>
                  </tr>                  
                </thead>
                <tbody>
                  @foreach($data->fields as $d)
                  <tr>
                    <td class="col-md-2">
                      <b>{{$d->field_number}}</b>
                    </td>
                    <td class="col-md-4">
                      <b>{{$d->name}}</b>
                    </td>
                    <td>{{$d->created_at}}</td>
                    <td>{{$d->updated_at}}</td>
                    <td>
                      <a href="/user/field/{{$d->id}}" class="btn btn-xs btn-success" data-id='{{$d->id}}' data-name='{{$d->name}}'><i class="fa fa-eye"></i></a>
                      <a href="#" class="edit btn btn-xs btn-primary" data-toggle='modal' data-id='{{$d->id}}' data-name='{{$d->name}}'><i class="fa fa-pencil"></i></a>
                      <a href="#" class="delete btn btn-xs btn-danger" data-id='{{$d->id}}'><i class="fa fa-times"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
      <!-- End Row-->
    </div>
    <!-- End Row-->
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- detail News Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id='createModal'>
  <div class="modal-dialog" role="document">
    <form action="/user/field/create" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <strong>Create Field</strong><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                <input type="text" class="form-control" placeholder="Name" name="name">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          {{csrf_field()}}
          <input type="hidden" name="channel_id" value="{{$data->id}}">
          <button type="button" class="btn btn-sm" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-sm btn-primary pull-right">Submit</button>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id='detailModal'>
    <div class="modal-dialog" role="document">
      <form action="/user/field/update/" method="post" id="edit-form">
        <div class="modal-content">
          <div class="modal-header">
            <strong>Field Detail</strong><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Name" name="name" id="edit-name">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              {{csrf_field()}}
              {!! method_field('patch') !!}
              <input type="hidden" name="id" value="" id="edit-id">
              <input type="hidden" name="channel_id" value="{{$data->id}}">
              <button type="button" class="btn btn-sm" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-sm btn-primary pull-right">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

@endsection
@push('js')
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>

<script type="text/javascript">
  $('.edit').click(function(){
      var name = $(this).data('name');
      var id = $(this).data('id');

      $('#edit-form').attr('action', '/user/field/update/'+id);
      $('#edit-name').val(name);
      $('#edit-id').val(id);
      
      $('#detailModal').modal();
  });

  $('.delete').click(function() {

    var id = $(this).data('id');

    var _token = '{{csrf_token()}}';

    bootbox.confirm("<b>Are you sure to delete this Field</b>?", function(result) {
      if (result) {
        toastr.options.timeOut = 0;
        toastr.options.extendedTimeOut = 0;
        toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Process...');
        toastr.options.timeOut = 5000;
        toastr.options.extendedTimeOut = 1000;
        $.post("/user/field/delete", {id: id, _token:_token, channel_id: {{$data->id}}})
        .done(function(result) {
          window.location.replace("/user/channel/{{$data->id}}");
        })
        .fail(function(result) {
          toastr.clear();
          toastr.error('Server Error! Please reload this page again.');
        });
      };
    });
  });
</script>
@endpush